var exec = require('cordova/exec');
exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'AmsmartWidgetPlugin', 'coolMethod', [arg0]);
};
exports.saveWidget = function (arg0, success, error) {
    exec(success, error, 'AmsmartWidgetPlugin', 'saveWidget', [arg0]);
};