package cordova.plugin.amsmartwidget;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public Context context;

    public DatabaseHelper(Context context) {
        super(context, "widgets", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE widgets (widget_id INTEGER PRIMARY KEY, token TEXT, control_mac TEXT, area_id TEXT, email TEXT, password TEXT, token_expires TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS widgets");
        onCreate(db);
    }

    public boolean addWidget(int widgetId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("widget_id", widgetId);

        return db.insert("widgets", null, contentValues) == -1 ? false : true;
    }

    public boolean deleteWidget(int widgetId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("widgets", "widget_id = " + widgetId, null) == -1 ? false : true;
    }

    public boolean addDataToWidget(int widgetId, String token, String mac, String areaId, String email, String password) {
        if (!existsWidget(widgetId)) return false;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("token", token);
        contentValues.put("control_mac", mac);
        contentValues.put("area_id", areaId);
        contentValues.put("email", email);
        contentValues.put("password", password);
        contentValues.put("token_expires", (System.currentTimeMillis() + 36000 * 1000) - 256);

        long result = db.update("widgets", contentValues, "widget_id = ?", new String[]{Integer.toString(widgetId)});

        // Update widgets after inserted into database
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, ControlWidget.class));
        ControlWidget myWidget = new ControlWidget();
        myWidget.onUpdate(context, AppWidgetManager.getInstance(context), ids);

        return result == -1 ? false : true;
    }

    public boolean updateWidgetToken(int widgetId, String token) {
        if (!existsWidget(widgetId)) return false;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("token", token);
        contentValues.put("token_expires", (System.currentTimeMillis() + 36000 * 1000) - 256);

        long result = db.update("widgets", contentValues, "widget_id = ?", new String[]{Integer.toString(widgetId)});
        return result == -1 ? false : true;
    }

    public WidgetModel getData(int widgetId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM widgets WHERE widget_id = " + widgetId;
        Cursor c = db.rawQuery(query, null);

        if (c != null && c.getCount() > 0) {
            return new WidgetModel(c);
        }
        c.close();
        return null;
    }

    public boolean widgetIsSet(int widgetId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM widgets WHERE widget_id = " + widgetId;
        Cursor c = db.rawQuery(query, null);

        boolean result = false;
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            if (c.getString(1) == null) {
                result = false;
            } else {
                result = true;
            }
        }
        c.close();
        return result;
    }

    public boolean existsWidget(int widgetId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM widgets WHERE widget_id = " + widgetId;
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.getCount() > 0) {
            c.close();
            return true;
        }
        c.close();
        return false;
    }
}