package cordova.plugin.amsmartwidget;

public class CrowAction {
    public static String ARM = "ARM";
    public static String DISARM = "DISARM";
    public static String STAY = "STAY";
    public static String REFRESH = "REFRESH";
    public static String LOGIN = "LOGIN";
    public static String OPEN_APP = "OPEN_APP";
    public static String SETUP_WIDGET = "SETUP_WIDGET";
}
