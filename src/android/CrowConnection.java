package cordova.plugin.amsmartwidget;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CrowConnection {
    private static final String BASE_URL = "https://api.crowcloud.xyz";
    private static final String CLIENT_ID = "t4WDssse5zzTbRlm8Vi7Evpa8ADz7g4xLiRnYOmx";
    private static final String CLIENT_SECRET = "qz8p9hcJNG6p3PJi7nZMva9M7IOOmTmhenPzMb8wV4aMQ4BIrmQpJVDIPyJK3mQ4LMYmhCwCoalQ4VRIaXXQBNvARrOpcrrO4PBxdyNiSmYOiurXULPk2C38oUVvBeAL";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void login(String email, String password, AsyncHttpResponseHandler responseHandler) {
        String url = "/o/token/";
        RequestParams params = new RequestParams();
        params.put("username", email);
        params.put("password", password);
        params.put("grant_type", "password");
        params.put("client_id", CLIENT_ID);
        params.put("client_secret", CLIENT_SECRET);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void changeAreaState(String action, String token, String mac, String area, AsyncHttpResponseHandler responseHandler) {
        String url = "/panels/" + mac + "/areas/" + area + "/";
        client.addHeader("Authorization", "Bearer " + token);

        RequestParams params = new RequestParams();
        params.put("state", action.toLowerCase());
        params.put("force", true);

        client.patch(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getArea(String token, String mac, String area, AsyncHttpResponseHandler responseHandler) {
        String url = "/panels/" + mac + "/areas/" + area + "/";
        client.addHeader("Authorization", "Bearer " + token);
        client.get(getAbsoluteUrl(url), responseHandler);
    }

    public static void getControl(String token, String mac, AsyncHttpResponseHandler responseHandler) {
        String url = "/panels/" + mac + "/";
        client.addHeader("Authorization", "Bearer " + token);
        client.get(getAbsoluteUrl(url), responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        String url = BASE_URL + relativeUrl;
        return url;
    }
}
