package cordova.plugin.amsmartwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RemoteViews;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ControlWidget extends AppWidgetProvider {
    public static Context CONTEXT;
    private static DatabaseHelper databaseHelper;
    private static boolean REFRESH_SET = false;

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        ControlWidget.CONTEXT = context;
        RemoteViews views = new RemoteViews(context.getPackageName(), getResources(context, "control_widget", "layout"));

        openDatabaseIfNeeded(context);

        if (!REFRESH_SET) {
            REFRESH_SET = true;
            refreshWidgets(context);
        }

        onSetup(context, views, appWidgetId);
        onOpenApp(context, views, appWidgetId);
        onRefresh(context, views, appWidgetId);

        // Show or hide buttons.
        if (!databaseHelper.existsWidget(appWidgetId)) {
            setWidgetLayout(false, views);
            databaseHelper.addWidget(appWidgetId);
        } else {
            if (databaseHelper.widgetIsSet(appWidgetId)) {
                setWidgetLayout(true, views);
                WidgetModel widgetModel = databaseHelper.getData(appWidgetId);
                if (widgetModel != null) {
                    onActionButtons(CrowAction.ARM, context, widgetModel, views);
                    onActionButtons(CrowAction.DISARM, context, widgetModel, views);
                    onActionButtons(CrowAction.STAY, context, widgetModel, views);
                    getControl(appWidgetManager, views, widgetModel);
                    getArea(context, appWidgetManager, views, widgetModel);
                }
            } else {
                setWidgetLayout(false, views);
            }
        }
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public void setWidgetLayout(boolean showButtons, RemoteViews views) {
        views.setViewVisibility(getResources(CONTEXT, "layoutButtons", "id"), showButtons ? View.VISIBLE : View.GONE);
        views.setViewVisibility(getResources(CONTEXT, "layoutSetup", "id"), showButtons ? View.GONE : View.VISIBLE);
    }

    public void getControl(AppWidgetManager appWidgetManager, RemoteViews views, WidgetModel widgetModel) {
        if (widgetModel.tokenExpired()) {
            CrowConnection.login(widgetModel.getEmail(), widgetModel.getPassword(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    configureNewToken(widgetModel, response);
                    getControl(appWidgetManager, views, widgetModel);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                }
            });
        } else {
            CrowConnection.getControl(widgetModel.getToken(), widgetModel.getMac(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        views.setTextViewText(getResources(CONTEXT, "textControl", "id"), response.getString("name"));
                        appWidgetManager.updateAppWidget(widgetModel.getId(), views);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                }
            });
        }
    }

    public void getArea(Context context, AppWidgetManager appWidgetManager, RemoteViews views, WidgetModel widgetModel) {
        if (widgetModel.tokenExpired()) {
            CrowConnection.login(widgetModel.getEmail(), widgetModel.getPassword(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    configureNewToken(widgetModel, response);
                    getArea(context, appWidgetManager, views, widgetModel);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                }
            });
        } else {
            CrowConnection.getArea(widgetModel.getToken(), widgetModel.getMac(), widgetModel.getArea(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String areaState = response.getString("state");

                        String action = null;
                        if (areaState.contains("arm")) action = CrowAction.ARM;
                        if (areaState.contains("disarm")) action = CrowAction.DISARM;
                        if (areaState.contains("stay")) action = CrowAction.STAY;
                        setButtonColors(context, action, widgetModel.getId());

                        views.setTextViewText(getResources(context, "textArea", "id"), response.getString("name"));
                        appWidgetManager.updateAppWidget(widgetModel.getId(), views);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                }
            });
        }
    }

    public static void onOpenApp(Context context, RemoteViews views, int appWidgetId) {
        Intent intent = new Intent(context, ControlWidget.class);
        intent.setAction(CrowAction.OPEN_APP);
        intent.putExtra("widgetId", appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, appWidgetId);
        views.setOnClickPendingIntent(getResources(context, "buttonOpenApp", "id"), pendingIntent);
    }

    public static void onSetup(Context context, RemoteViews views, int appWidgetId) {
        Intent intent = new Intent(context, ControlWidget.class);
        intent.setAction(CrowAction.SETUP_WIDGET);
        intent.putExtra("widgetId", appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, appWidgetId);
        views.setOnClickPendingIntent(getResources(context, "setupButton", "id"), pendingIntent);
    }

    public static void onRefresh(Context context, RemoteViews views, int appWidgetId) {
        Intent intent = new Intent(context, ControlWidget.class);
        intent.setAction(CrowAction.REFRESH);
        intent.putExtra("widgetId", appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, appWidgetId);
        views.setOnClickPendingIntent(getResources(context, "buttonRefresh", "id"), pendingIntent);
    }

    public static void onActionButtons(String action, Context context, WidgetModel widgetModel, RemoteViews views) {
        Intent intent = new Intent(context, ControlWidget.class);
        intent.setAction(action);
        intent.putExtra("widgetId", widgetModel.getId());
        intent.putExtra("token", widgetModel.getToken());
        intent.putExtra("mac", widgetModel.getMac());
        intent.putExtra("area", widgetModel.getArea());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, widgetModel.getId());

        int buttonId = -1;
        if (action == CrowAction.ARM) buttonId = getResources(context, "buttonArm", "id");
        if (action == CrowAction.DISARM) buttonId = getResources(context, "buttonDisarm", "id");
        if (action == CrowAction.STAY) buttonId = getResources(context, "buttonStay", "id");

        views.setOnClickPendingIntent(buttonId, pendingIntent);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        for (int appWidgetId : appWidgetIds) {
            openDatabaseIfNeeded(context);
            boolean deleted = databaseHelper.deleteWidget(appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        if (intent.getAction().equals(CrowAction.SETUP_WIDGET)) {
            int widgetId = Integer.parseInt(intent.getExtras().get("widgetId").toString());
            Intent i = context.getPackageManager().getLaunchIntentForPackage(CONTEXT.getPackageName());
            i.putExtra("SETUP_WIDGET", widgetId);
            context.startActivity(i);
        } else if (intent.getAction().equals(CrowAction.OPEN_APP)) {
            Intent i = context.getPackageManager().getLaunchIntentForPackage(CONTEXT.getPackageName());
            context.startActivity(i);
        } else if (intent.getAction().equals(CrowAction.REFRESH)) {
            int widgetId = Integer.parseInt(intent.getExtras().get("widgetId").toString());
            updateAppWidget(context, appWidgetManager, widgetId);
        } else if (intent.getAction().equals(CrowAction.ARM)) {
            int widgetId = Integer.parseInt(intent.getExtras().get("widgetId").toString());
            receiveStateAction(context, widgetId, CrowAction.ARM);
        } else if (intent.getAction().equals(CrowAction.DISARM)) {
            int widgetId = Integer.parseInt(intent.getExtras().get("widgetId").toString());
            receiveStateAction(context, widgetId, CrowAction.DISARM);
        } else if (intent.getAction().equals(CrowAction.STAY)) {
            int widgetId = Integer.parseInt(intent.getExtras().get("widgetId").toString());
            receiveStateAction(context, widgetId, CrowAction.STAY);
        }
    }

    private void configureNewToken(WidgetModel widgetModel, JSONObject response) {
        try {
            String token = response.getString("access_token");
            widgetModel.setToken(token);
            widgetModel.setTokenExpires(Long.toString((System.currentTimeMillis() + 36000 * 1000) - 256));
            openDatabaseIfNeeded(null);
            databaseHelper.updateWidgetToken(widgetModel.getId(), token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void receiveStateAction(Context context, int widgetId, String action) {
        openDatabaseIfNeeded(context);
        WidgetModel widgetModel = databaseHelper.getData(widgetId);

        if (widgetModel.tokenExpired()) {
            CrowConnection.login(widgetModel.getEmail(), widgetModel.getPassword(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    configureNewToken(widgetModel, response);
                    receiveStateAction(context, widgetId, action);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                    ;
                }
            });
        } else {
            CrowConnection.changeAreaState(action, widgetModel.getToken(), widgetModel.getMac(), widgetModel.getArea(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    setButtonColors(context, action, widgetId);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    System.err.println(statusCode);
                }
            });
        }
    }

    private static void setButtonColors(Context context, String actionType, int widgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(), getResources(context, "control_widget", "layout"));
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        if (actionType == CrowAction.ARM) {
            views.setInt(getResources(context, "buttonArm", "id"), "setBackgroundResource", getResources(context, "button_arm_active", "drawable"));
            views.setInt(getResources(context, "buttonArm", "id"), "setColorFilter", 0xffffffff);
            views.setInt(getResources(context, "buttonDisarm", "id"), "setBackgroundResource", getResources(context, "button_disarm_inactive", "drawable"));
            views.setInt(getResources(context, "buttonDisarm", "id"), "setColorFilter", 0xffaaaaaa);
            views.setInt(getResources(context, "buttonStay", "id"), "setBackgroundResource", getResources(context, "button_stay_inactive", "drawable"));
            views.setInt(getResources(context, "buttonStay", "id"), "setColorFilter", 0xffaaaaaa);
        } else if (actionType == CrowAction.DISARM) {
            views.setInt(getResources(context, "buttonArm", "id"), "setBackgroundResource", getResources(context, "button_arm_inactive", "drawable"));
            views.setInt(getResources(context, "buttonArm", "id"), "setColorFilter", 0xffaaaaaa);
            views.setInt(getResources(context, "buttonDisarm", "id"), "setBackgroundResource", getResources(context, "button_disarm_active", "drawable"));
            views.setInt(getResources(context, "buttonDisarm", "id"), "setColorFilter", 0xffffffff);
            views.setInt(getResources(context, "buttonStay", "id"), "setBackgroundResource", getResources(context, "button_stay_inactive", "drawable"));
            views.setInt(getResources(context, "buttonStay", "id"), "setColorFilter", 0xffaaaaaa);
        } else if (actionType == CrowAction.STAY) {
            views.setInt(getResources(context, "buttonArm", "id"), "setBackgroundResource", getResources(context, "button_arm_inactive", "drawable"));
            views.setInt(getResources(context, "buttonArm", "id"), "setColorFilter", 0xffaaaaaa);
            views.setInt(getResources(context, "buttonDisarm", "id"), "setBackgroundResource", getResources(context, "button_disarm_inactive", "drawable"));
            views.setInt(getResources(context, "buttonDisarm", "id"), "setColorFilter", 0xffaaaaaa);
            views.setInt(getResources(context, "buttonStay", "id"), "setBackgroundResource", getResources(context, "button_stay_active", "drawable"));
            views.setInt(getResources(context, "buttonStay", "id"), "setColorFilter", 0xffffffff);
        }
        appWidgetManager.updateAppWidget(widgetId, views);
    }

    public static void openDatabaseIfNeeded(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context != null ? context : CONTEXT);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    }

    @Override
    public void onEnabled(Context context) {

    }

    private void refreshWidgets(Context context) {
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, ControlWidget.class));
        if (ids.length < 1) {
            REFRESH_SET = false;
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                onUpdate(context, appWidgetManager, ids);
                refreshWidgets(context);
            }
        }, 1000 * 60);
    }

    @Override
    public void onDisabled(Context context) {

    }

    @Override
    public void onRestored(Context context, int[] oldWidgetIds, int[] newWidgetIds) {
        super.onRestored(context, oldWidgetIds, newWidgetIds);
    }

    private static int getResources(Context context, String name, String type) {
        return context.getResources().getIdentifier(name, type, context.getPackageName());
    }
}