package cordova.plugin.amsmartwidget;

import android.database.Cursor;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WidgetModel {

    private int id;
    private String token;
    private String mac;
    private String area;
    private String email;
    private String password;
    private String tokenExpires;

    public WidgetModel() {
    }

    public WidgetModel(String widget) throws JSONException {
        JSONObject object = new JSONObject(widget);
        this.id = object.getInt("id");
        this.token = object.getString("token");
        this.mac = object.getString("mac");
        this.area = object.getString("area");
        this.email = object.getString("email");
        this.password = object.getString("password");
        this.tokenExpires = object.getString("tokenExpires");
    }

    public WidgetModel(Cursor c) {
        c.moveToFirst();
        this.id = c.getInt(0);
        this.token = c.getString(1);
        this.mac = c.getString(2);
        this.area = c.getString(3);
        this.email = c.getString(4);
        this.password = c.getString(5);
        this.tokenExpires = c.getString(6);
        c.close();
    }

    public WidgetModel(int id, String token, String mac, String area, String email, String password, String tokenExpires) {
        this.id = id;
        this.token = token;
        this.mac = mac;
        this.area = area;
        this.email = email;
        this.password = password;
        this.tokenExpires = tokenExpires;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(String tokenExpires) {
        this.tokenExpires = tokenExpires;
    }

    public boolean tokenExpired() {
        Date now = new Date(System.currentTimeMillis());
        long expire = Long.parseLong(this.tokenExpires);
        Date expireDate = new Date(expire);

        String nowString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(now);
        String expireString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(expireDate);
        System.out.println("Date expire: [NOW] " + nowString + " | [EXPIRE] " + expireString + " -> " + now.after(expireDate));

        return now.after(expireDate);
    }

    @NonNull
    @Override
    public String toString() {
        JSONObject object = new JSONObject();
        try {
            object.put("id", id);
            object.put("token", token);
            object.put("mac", mac);
            object.put("area", area);
            object.put("email", email);
            object.put("password", password);
            object.put("tokenExpires", tokenExpires);
        } catch (JSONException e) {
            e.printStackTrace();
            return "-";
        }
        return object.toString();
    }
}
