package cordova.plugin.amsmartwidget;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

public class AmsmartWidgetPlugin extends CordovaPlugin {

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.d("TAGAmsmartWidgetPlugin", "Initializing AmsmartWidgetPlugin");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        } else if (action.equals("saveWidget")) {
            String data = args.getString(0);
            this.saveWidget(data, callbackContext);
            return true;
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void saveWidget(String data, CallbackContext callbackContext) {
        if (data != null && data.length() > 0) {
            Log.d("TAGAmsmartWidgetPlugin", data);
            String[] values = data.split("-");
            DatabaseHelper databaseHelper = new DatabaseHelper(ControlWidget.CONTEXT);
            databaseHelper.addDataToWidget(Integer.parseInt(values[0]), values[1], values[2], values[3], values[4], values[5]);
            callbackContext.success(data);
        } else {
            callbackContext.error("Error...");
        }
    }
}
